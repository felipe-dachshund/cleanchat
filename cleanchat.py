#!/usr/bin/python3
# Copyright 2021 Felipe Oliveira da Silva Netto, Aécio Beltrão Rodrigues Castro
# Copyright 2020 Felipe Oliveira da Silva Netto
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>

import re
from argparse import ArgumentParser
from os import listdir, replace, chdir
from sys import stdout, stderr, argv
from tempfile import NamedTemporaryFile
from gooey import Gooey, GooeyParser

if __name__ != '__main__':
    raise Exception("cleanchat isn't a module, it must be called "
                    "independently!")
VERSION = '1'
DESCRIPTION = 'Clean .cha files'
EPILOG = 'cleanchat v' + VERSION + ', Felipe Oliveira da Silva Netto' + ', Aécio Beltrão Rodrigues Castro'
PAUSES = {'SHORT': '(.)', 'MEDIUM': '(..)', 'LONG': '(...)', 'NONE': None}

def parseArguments():
    parser = ArgumentParser(description=DESCRIPTION, epilog=EPILOG)
    parser.add_argument('input_files', metavar='FILE', nargs='*',
                        help='zero or more CHAT files as input. When missing, '
                        'it uses all .cha files from working directory.')
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='Verbose output')
    parser.add_argument('-o', dest='out_pattern', metavar='FILE_PATTERN',
                        default='%f.cha', help='Output file name (in case of '
                        'one input file) or output filename pattern. For a '
                        'pattern, %%f denotes the corresponding input '
                        'filename (w/o extension), and %%%% denotes the %% '
                        'character. When missing, it overwrites the input '
                        'files.')
    parser.add_argument('--change-hashes', metavar='PAUSE', default=None,
                        help="Replace '#' according to PAUSE. PAUSE can be "
                        "'short' for short pause symbol '" + PAUSES['SHORT'] +
                        "', 'medium' for medium pause symbol '" +
                        PAUSES['MEDIUM'] + "', 'long' for long pause symbol '" +
                        PAUSES['LONG'] + "' or None for no replacement. Default"
                        " is None.")
    parser.add_argument('-e', '--exclude', metavar='FILE', nargs='+',
                        help='Exclude FILEs from input file list, so not '
                        'cleaning them. It might come preferably after '
                        'positional arguments.')

    args = parser.parse_args()
    if len(args.input_files) == 0:
        args.input_files = list(filter(lambda x: x[-4:] == '.cha', listdir()))
    if args.exclude:
        for file in args.exclude:
            print(file)
            try:
                args.input_files.remove(file)
            except ValueError as e:
                del e
        del args.exclude

    args.out_pattern = args.out_pattern.split('%%')

    if args.change_hashes:
        args.change_hashes = args.change_hashes.upper()
        if args.change_hashes not in PAUSES:
            raise Exception('Unvalid value to option --change-hashes')
        args.change_hashes = PAUSES[args.change_hashes]

    return args

def parseGooeyArguments():
    parser = GooeyParser(description=DESCRIPTION, epilog=EPILOG)
    parser.add_argument(dest='input_dir', metavar='Input Directory',
                        help='Select input directory', widget='DirChooser')
    parser.add_argument('--out-pattern', metavar='Output Pattern',
                        default='%f.cha', help='Output file name (in case of '
                        'one input file) or output filename pattern. For a '
                        'pattern, %f denotes the corresponding input filename '
                        '(w/o extension), and %% denotes the % character. '
                        'Default is to overwrite input files.')
    hash_options = PAUSES.copy()
    hash_options['NONE'] = "Don't replace"
    group = parser.add_argument('--change-hashes',
        metavar='Replace Hash Symbol', help='Replace # to:',
        choices=hash_options.values(), default=None)

    args = parser.parse_args()
    args.verbose = True
    args.input_files = list(filter(lambda x: x[-4:] == '.cha', listdir(args.input_dir)))

    args.out_pattern = args.out_pattern.split('%%')

    if args.change_hashes == hash_options['NONE']:
        args.change_hashes = None

    return args

def message(title, lineNo, line, more='', type='info', abort=0):
    type2 = type.upper()[:3]
    if type2 == 'ERR':
        msg = 'ERR: '
        msgFile = stderr
    elif type2 in ('WAR', 'WRN'):
        msg = 'WRN: '
        msgFile = stderr
    else:
        msg = 'INFO: '
        msgFile = stdout
    msg += title + f" (at line {lineNo})\n" + line
    if line == '' or line[-1] != '\n':
        msg += '\n'
    msg += more
    if more and more[-1] != '\n':
        msg += '\n'
    print(msg, file=msgFile)
    if abort:
        exit(abort)

blanks = re.compile('\s+') # Match any amount of whitespace.
blanksAfterFirstChar = re.compile('(?<=^.)\s+') # Match whitespace preceded by any character, but only if that character is at the start of the string. '?<=' is a positive lookbehind, which doesn't include what it matches in the result.
tierCode = re.compile('^(\*[A-Z]{3}|%[a-z]{3}):') # Match three-letter codes (plus colon).
pattern3warn = re.compile('^([A-z]{3,}):') # Match bigger-than-normal three-letter codes (plus colon).
pattern3warn2 = re.compile('^[A-Z]{3,}\b') # Match an uppercase word with 3+ letters at beginning of line.
pattern4 = re.compile('^\w+:') # Match one or more word characters at the start of the string (plus colon).

def cleanFile(filename, args):
    with open(filename) as file:
        if args.verbose:
            print('Cleaning', filename + '...')
        filename = filename.split('.')[0]
        outname = '%'.join(map(lambda x: x.replace('%f', filename),
                           args.out_pattern))
        with NamedTemporaryFile(mode='w', newline='\n', suffix='.cha', dir='.',
                                delete=False) as outfile:
            line_no = 0
            for line in file:
                if line.isspace():
                    continue
                line = line.strip()
                line = blanks.sub(' ', line) # Replace whitespace with a single space.
                if args.change_hashes:
                    line = line.replace('#', args.change_hashes)
                line = line.replace(',,', '„') # Replace double comma.

                if line[0] in ('@', '*', '%'):
                    line = blanksAfterFirstChar.sub('', line) # Eliminate space (if any) between the symbol and the code that follows it.
                    if line[:4].upper() == '@END':
                        outfile.write('@End\n')
                        break
                    lines = line.split(':')
                    lines[0] = lines[0].rstrip()
                    if len(lines) > 1:
                        lines[1] = '\t' + lines[1].lstrip()
                    line = ':'.join(lines)
                    if not line[0] is "@" and not tierCode.match(line):
                        message('Invalid code!', line_no + 1, line, type='error')
                elif pattern3warn.match(line):
                    if pattern3warn.match(line).group().isupper():
                        message('Maybe there is a * missing?', line_no + 1, line, type='warning')
                    elif pattern3warn.match(line).group().islower():
                        message('Maybe there is a % missing?', line_no + 1, line, type='warning')
                    else:
                        message('Maybe there is a % or * missing?', line_no + 1, line,
                                more='Could not determine wether a % or a * due to mixed case.', type='warning')
                else:
                    if pattern3warn2.match(line):
                        message('Maybe there is a * and a colon missing?', line_no + 1, line,
                                more="It's OK, I will assume it's just an acronym.", type='warning')
                    line = '\t' + line
                outfile.write(line + '\n')

                line_no += 1
            outfile.close()
            replace(outfile.name, outname)
        if args.verbose:
            print('saved in', outname)

if len(argv) < 2 or '--ignore-gooey' in argv:
    @Gooey
    def main():
        args = parseGooeyArguments()
        chdir(args.input_dir)
        for fname in args.input_files:
            cleanFile(fname, args)
else:
    def main():
        args = parseArguments()

        for fname in args.input_files:
            cleanFile(fname, args)

main()
